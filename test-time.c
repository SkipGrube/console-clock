#include <stdio.h>
#include <time.h>
#include <string.h>
#include <curses.h>
#include "big-chars.h"

void drawtime(char numtime[8], char oldtime[8], int X[8], int Y[8], int size, int color, char printchar)
{
	/*
	This complicated bit of logic is divided up for greater clarity.  First, check to see if a particular
	character has changed.  If it has, find the one that was there before and erase it.  Then draw the new
	character
	*/
	int c;
	for (c=0; c <= 7; c++) {
		if (oldtime[c]!=numtime[c]) {
		
			if (oldtime[c]=='1')
				one(X[c], Y[c], size, color, ' ');
			else if (oldtime[c]=='2')
				two(X[c], Y[c], size, color, ' ');
			else if (oldtime[c]=='3')
				three(X[c], Y[c], size, color, ' ');
			else if (oldtime[c]=='4')
				four(X[c], Y[c], size, color, ' ');
			else if (oldtime[c]=='5')
				five(X[c], Y[c], size, color, ' ');
			else if (oldtime[c]=='6')
				six(X[c], Y[c], size, color, ' ');
			else if (oldtime[c]=='7')
				seven(X[c], Y[c], size, color, ' ');
			else if (oldtime[c]=='8')
				eight(X[c], Y[c], size, color, ' ');
			else if (oldtime[c]=='9')
				nine(X[c], Y[c], size, color, ' ');
			else if (oldtime[c]=='0')
				zero(X[c], Y[c], size, color, ' ');
			else if (oldtime[c]==':')
				colon(X[c], Y[c], size, color, ' ');


			if (numtime[c]=='1')
				one(X[c], Y[c], size, color, printchar);
			else if (numtime[c]=='2')
				two(X[c], Y[c], size, color, printchar);
			else if (numtime[c]=='3')
				three(X[c], Y[c], size, color, printchar);
			else if (numtime[c]=='4')
				four(X[c], Y[c], size, color, printchar);
			else if (numtime[c]=='5')
				five(X[c], Y[c], size, color, printchar);
			else if (numtime[c]=='6')
				six(X[c], Y[c], size, color, printchar);
			else if (numtime[c]=='7')
				seven(X[c], Y[c], size, color, printchar);
			else if (numtime[c]=='8')
				eight(X[c], Y[c], size, color, printchar);
			else if (numtime[c]=='9')
				nine(X[c], Y[c], size, color, printchar);
			else if (numtime[c]=='0')
				zero(X[c], Y[c], size, color, printchar);
			else if (numtime[c]==':')
				colon(X[c], Y[c], size, color, printchar);
		}
			
			
	}

}

char *updatetime()
{
	time_t rawtime;
	struct tm * timeinfo;
		
	char *temp;
	time (&rawtime);
	timeinfo = localtime (&rawtime);
//	printf("%s \n", asctime(timeinfo));
	temp=asctime(timeinfo);
	return temp;

}


void displayHelp() {
        printf("Console Clock 0.2.2 ------------------------ \n\n");
	printf("Options: \n");
	printf("-h:  Display this help menu\n");
	printf("-c [char]:  Use [char] as the character for displaying time digits\n");
	printf("-s [size]:  Number indicating the relative size of the digits (the default is 4)\n");
        printf("\n\n");
	printf("This program and all associated files were created by Skip Grube (team77mail@yahoo.com)\n");
	printf("All code is licensed under the Lesser GNU Public License (LGPL)\n\n");
	
	exit(0);
}



int main(int argc, char *argv[])
{
	int X[8], Y[8];
	char timevar[26];
	char oldtime[8];
	char numtime[8];
	int color;
	char printchar;
	int size;
	int Ymax,Xmax;
	
	strcpy(timevar,updatetime()); //get initial time into timevar (char[] format)
	
	
	//test case defaults
	size=4;
	color=0;
	printchar='\n';
	
	/*parse stuff @ command line*/
	int c=0;
	for (c=0; c < argc; c++) {
		if (argv[c][0] == '-' && argv[c][1] == 'c') {
	
			if ((c+1 < argc) && strlen(argv[c+1]) == 1)
				printchar = argv[c+1][0];
			else
				printf("Error: long or nonexistent argument for the -c (character) option!\n");
		}
		
		if (argv[c][0] == '-' && argv[c][1] == 'h') {
			displayHelp();
		}
		
		if (argv[c][0] == '-' && argv[c][1] == 's') {
			if ((c+1 < argc) && strlen(argv[c+1]) == 1 && atoi(&argv[c+1][0]) < 10 && atoi(&argv[c+1][0]) > 0 ) {
				size = atoi(&argv[c+1][0]);
			}
		}
		
		
	}

	
	initscr();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);
	getmaxyx(stdscr,Ymax,Xmax);
	
	
	int separationX=(int)((Xmax-(17.75*size))/2);	
	int separationY=(int)((Ymax-(3*size))/2);	
	if (separationX < 0 || separationY < 0) {
		endwin();
		printf("Error: Given text size is too big for detected screen.  Exiting... \n\n");
		return 1;
	}
	
	
	for (c=0; c <= 7; c++) {
		X[c]=separationX+((int)c*2*size);
		Y[c]=separationY;
	}
	
	//begin main loop
	int i=0;	
	while(1) {
		strcpy(timevar,updatetime());  //use to grab the full time in char[] format
		for (c=0; c <= 7; c++)  { 
			oldtime[c]=numtime[c];
			numtime[c]=timevar[c+11];
		}
		
		drawtime(numtime, oldtime, X, Y, size, color, printchar);
		wrefresh(stdscr);
		/*note that this sleep function is very important!  otherwise the program runs the loop 
		zillions of times per second and eats up all the processor power*/
		sleep(1);	
//		getchar();
//		i++;
	}	
  		endwin();
		printf("%d     %d \n \n",separationX,separationY);
	
	//end main loop
		
	
	
	
	//printf("%c  %c \n",timevar[11],timevar[13]);
	
	return 0;
}

