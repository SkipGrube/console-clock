/*
Author: Skip Grube (team77mail@yahoo.com)
Version: 0.2
9/14/2006

This is a "library" of sorts for displaying digital clock-like numbers in a specified format. 
(usually larger, made up of bunches of characters)  It will be used in my console digital clock 
application, (upcoming) but others may find it useful.  Simply #include this file (and make sure 
you have the curses development lib installed!) to access the functions for displaying large 
digital numbers made of characters.  This library has no official name yet.
*/



#include <curses.h>
#include <stdio.h>
#include "big-chars.h"

/*
the following 7 functions (all void) are a way of drawing the 7 possible "lines"
on a standard digital clock, except you can pass any location on the screen as well as 
the color and size... the variables for these are passed along from the call to the number.

The number then calls these functions to "construct" itself.

The layout of a standard digital clock with respect to these fuctions can 
be determined with the following (rather crude) diagram:
		
		pos1
	 ---------
pos4|		   | pos5
	 |	pos2  |
	 ---------
pos6|		   | pos7
	 |	pos3  |
	 ---------

*/

void pos1(int X, int Y, int size, int color, char printchar)
{
	move(Y,X);
	
	int i,j;
	for (i=1; i <= (int)size*3/5+2; i++)  {
		for (j=1; j <= (int)size*1.5; j++)
			addch(printchar);
	move(Y+i-1,X);
	}

}

void pos2(int X, int Y, int size, int color, char printchar)
{
	int i,j;
	Y=Y+(int)size*6/5+1;
	move(Y,X);
	for (i=1; i <= (int)size*3/5+2; i++)  {
		for (j=1; j <= (int)size*1.5; j++)
			addch(printchar);
	move(Y+i-1,X);
	}
}

void pos3(int X, int Y, int size, int color, char printchar)
{
	int i,j;
	Y=Y+(int)size*12/5+1;
	move(Y,X);
	for (i=1; i <= (int)size*3/5+2; i++)  {
		for (j=1; j <= (int)size*1.5; j++)
			addch(printchar);
	move(Y+i-1,X);
	}
}

void pos4(int X, int Y, int size, int color, char printchar)
{
	int i,j;
	move(Y,X);
	for (i=1; i <= (int)size*9/5+2; i++)  {
		for (j=1; j <= (int)size/2; j++)
			addch(printchar);
	move(Y+i-1,X);
	}
}

void pos5(int X, int Y, int size, int color, char printchar)
{
	int i,j;
	X=X+(int)size;
	move(Y,X);
	for (i=1; i <= (int)size*9/5+2; i++)  {
		for (j=1; j <= (int)size/2; j++)
			addch(printchar);
	move(Y+i-1,X);
	}
}

void pos6(int X, int Y, int size, int color, char printchar)
{
	int i,j;
	Y=Y+(int)size*6/5+1;
	move(Y,X);
	for (i=1; i <= (int)size*9/5+2; i++)  {
		for (j=1; j <= (int)size/2; j++)
			addch(printchar);
	move(Y+i-1,X);
	}
}

void pos7(int X, int Y, int size, int color, char printchar)
{
	int i,j;
	Y=Y+(int)size*6/5+1;
	X=X+(int)size;
	move(Y,X);
	for (i=1; i <= (int)size*9/5+2; i++)  {
		for (j=1; j <= (int)size/2; j++)
			addch(printchar);
	move(Y+i-1,X);
	}
}

void one(int X,int Y,int size,int color, char printchar)
{
 	
 	
 	if (printchar=='\n')
		printchar='1';
	move(Y,X);
	pos5(X,Y,size,color,printchar);
	pos7(X,Y,size,color,printchar);
	
}

void two(int X, int Y, int size, int color, char printchar)
{
	if (printchar=='\n')
		printchar='2';
	
	move(Y,X);
	pos1(X,Y,size,color,printchar);
	pos5(X,Y,size,color,printchar);
	pos2(X,Y,size,color,printchar);
	pos6(X,Y,size,color,printchar);
	pos3(X,Y,size,color,printchar);

}

void three(int X, int Y, int size, int color, char printchar)
{
	if (printchar=='\n')
		printchar='3';
	
	move(Y,X);
	pos1(X,Y,size,color,printchar);
	pos5(X,Y,size,color,printchar);
	pos2(X,Y,size,color,printchar);
	pos7(X,Y,size,color,printchar);
	pos3(X,Y,size,color,printchar);

}

void four(int X, int Y, int size, int color, char printchar)
{
	if (printchar=='\n')
		printchar='4';
	
	move(Y,X);
	pos4(X,Y,size,color,printchar);
	pos5(X,Y,size,color,printchar);
	pos2(X,Y,size,color,printchar);
	pos7(X,Y,size,color,printchar);
}

void five(int X, int Y, int size, int color, char printchar)
{
	if (printchar=='\n')
		printchar='5';
	
	move(Y,X);
	pos1(X,Y,size,color,printchar);
	pos4(X,Y,size,color,printchar);
	pos2(X,Y,size,color,printchar);
	pos7(X,Y,size,color,printchar);
	pos3(X,Y,size,color,printchar);

}

void six(int X, int Y, int size, int color, char printchar)
{
	if (printchar=='\n')
		printchar='6';
	
	move(Y,X);
	pos1(X,Y,size,color,printchar);
	pos4(X,Y,size,color,printchar);
	pos7(X,Y,size,color,printchar);
	pos2(X,Y,size,color,printchar);
	pos6(X,Y,size,color,printchar);
	pos3(X,Y,size,color,printchar);

}

void seven(int X, int Y, int size, int color, char printchar)
{
	if (printchar=='\n')
		printchar='7';
	
	move(Y,X);
	pos1(X,Y,size,color,printchar);
	pos5(X,Y,size,color,printchar);
	pos7(X,Y,size,color,printchar);
}

void eight(int X, int Y, int size, int color, char printchar)
{
	if (printchar=='\n')
		printchar='8';
	
	move(Y,X);
	pos1(X,Y,size,color,printchar);
	pos4(X,Y,size,color,printchar);
	pos7(X,Y,size,color,printchar);
	pos2(X,Y,size,color,printchar);
	pos6(X,Y,size,color,printchar);
	pos3(X,Y,size,color,printchar);
	pos5(X,Y,size,color,printchar);
}

void nine(int X, int Y, int size, int color, char printchar)
{
	if (printchar=='\n')
		printchar='9';
	
	move(Y,X);
	pos1(X,Y,size,color,printchar);
	pos4(X,Y,size,color,printchar);
	pos7(X,Y,size,color,printchar);
	pos2(X,Y,size,color,printchar);	
	pos5(X,Y,size,color,printchar);
}

void zero(int X, int Y, int size, int color, char printchar)
{
	if (printchar=='\n')
		printchar='0';
	
	move(Y,X);
	pos1(X,Y,size,color,printchar);
	pos4(X,Y,size,color,printchar);
	pos7(X,Y,size,color,printchar);
	pos6(X,Y,size,color,printchar);
	pos3(X,Y,size,color,printchar);
	pos5(X,Y,size,color,printchar);
	

}

void colon(int X, int Y, int size, int color, char printchar)
{
	/*
	the colon (":") character is not made up of the posN functions, and therefore
	is drawn directly from this function when it is called
	*/
	if (printchar=='\n')
		printchar=':';
	
	int i,j;
	X=X+(int)size*4/5;
	Y=Y+(int)size;
	move(Y,X);
	for (i=1; i <= (int)size*3/5; i++)  {
		for (j=1; j <= (int)size/2; j++)
			addch(printchar);
	move(Y+i-1,X);
	}

	Y=Y+(int)size;
	move(Y,X);
	for (i=1; i <= (int)size*3/5; i++)  {
		for (j=1; j <= (int)size/2; j++)
			addch(printchar);
	move(Y+i-1,X);
	}
	
	
}

/*
int main()
{
	initscr();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);
	
	zero(4,4,6,0,'\n');
	wrefresh(stdscr);
	getch();
	zero(4,4,6,0,' ');
	
	
	one(4,4,6,0,'\n');
	wrefresh(stdscr);
	getch();
	one(4,4,6,0,' ');
	

	two(4,4,6,0,'\n');
	wrefresh(stdscr);
	getch();
	two(4,4,6,0,' ');
	
	three(4,4,6,0,'\n');
	wrefresh(stdscr);
	getch();
	three(4,4,6,0,' ');
	
	four(4,4,6,0,'\n');
	wrefresh(stdscr);
	getch();
	four(4,4,6,0,' ');
	
	five(4,4,6,0,'\n');
	wrefresh(stdscr);
	getch();
	five(4,4,6,0,' ');	
	
	six(4,4,6,0,'\n');
	wrefresh(stdscr);
	getch();
	six(4,4,6,0,' ');
	
	seven(4,4,6,0,'\n');
	wrefresh(stdscr);
	getch();
	seven(4,4,6,0,' ');
	
	eight(4,4,6,0,'\n');
	wrefresh(stdscr);
	getch();
	eight(4,4,6,0,' ');
	
	nine(4,4,10,0,'\n');
	colon(20,4,10,0,'\n');
	wrefresh(stdscr);
	getch();
	nine(4,4,6,0,' ');
	
	wrefresh(stdscr);
	int y,x;
	getmaxyx(stdscr,y,x);
	endwin();
	printf("%d     %d \n", x,y);
	return 0;
}
*/
