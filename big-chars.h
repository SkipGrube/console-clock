/*
Author: Skip Grube (team77mail@yahoo.com)
Version: 0.2
9/14/2006

This is a "library" of sorts for displaying digital clock-like numbers in a specified format. 
(usually larger, made up of bunches of characters)  It will be used in my console digital clock 
application, (upcoming) but others may find it useful.  Simply #include this file (and make sure 
you have the curses development lib installed!) to access the functions for displaying large 
digital numbers made of characters.  This library has no official name yet.
*/

/*
#ifndef __BIGCHARS_H
#define __BIGCHARS_H
*/
/*
the following 7 functions (all void) are a way of drawing the 7 possible "lines"
on a standard digital clock, except you can pass any location on the screen as well as 
the color and size... the variables for these are passed along from the call to the number.

The number then calls these functions to "construct" itself.

The layout of a standard digital clock with respect to these fuctions can 
be determined with the following (rather crude) diagram:
		
		pos1
	 ---------
pos4|		   | pos5
	 |	pos2  |
	 ---------
pos6|		   | pos7
	 |	pos3  |
	 ---------

*/

void pos1(int X, int Y, int size, int color, char printchar);
void pos2(int X, int Y, int size, int color, char printchar);
void pos3(int X, int Y, int size, int color, char printchar);
void pos4(int X, int Y, int size, int color, char printchar);
void pos5(int X, int Y, int size, int color, char printchar);
void pos6(int X, int Y, int size, int color, char printchar);
void pos7(int X, int Y, int size, int color, char printchar);

void one(int X,int Y,int size,int color, char printchar);
void two(int X,int Y,int size,int color, char printchar);
void three(int X,int Y,int size,int color, char printchar);
void four(int X,int Y,int size,int color, char printchar);
void five(int X,int Y,int size,int color, char printchar);
void six(int X,int Y,int size,int color, char printchar);
void seven(int X,int Y,int size,int color, char printchar);
void eight(int X,int Y,int size,int color, char printchar);
void nine(int X,int Y,int size,int color, char printchar);
void zero(int X,int Y,int size,int color, char printchar);
void colon(int X,int Y,int size,int color, char printchar);

/*#endif*/
